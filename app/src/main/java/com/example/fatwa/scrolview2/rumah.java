package com.example.fatwa.scrolview2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class rumah extends AppCompatActivity {

    Button btn_navigasi;
    String goolgeMap = "com.google.android.apps.maps";
    Uri gmmIntentUri;
    Intent mapIntent;
    String panorama = "-6.3166041,108.2902286";
    String cimanuk = "-6.4820993,108.2256605";
    String burbacek ="-6.3331657,108.3197703";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rumah);

        btn_navigasi    = (Button) findViewById(R.id.button1);


        btn_navigasi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                gmmIntentUri = Uri.parse("google.navigation:q=" + panorama);


                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);



                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(rumah.this, "Google Maps Belum Terinstal. Install Terlebih dahulu.",
                            Toast.LENGTH_LONG).show();
                }
            }

        });
        //EditText

        Button diall = (Button) findViewById(R.id.button2);
        //ketika menekan tombol call maka akan melakukan dial
        diall.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //number = inputan dari editText
                String toDial = "tel:" + "087727527077";

                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(toDial)));

            }
        });
        btn_navigasi    = (Button) findViewById(R.id.button3);


        btn_navigasi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                gmmIntentUri = Uri.parse("google.navigation:q=" +cimanuk);


                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);



                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(rumah.this, "Google Maps Belum Terinstal. Install Terlebih dahulu.",
                            Toast.LENGTH_LONG).show();
                }
            }

        });
        //EditText

        Button dial = (Button) findViewById(R.id.button4);
        //ketika menekan tombol call maka akan melakukan dial
        dial.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //number = inputan dari editText
                String toDial = "tel:" + "08122376642";

                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(toDial)));

            }
        });

        btn_navigasi    = (Button) findViewById(R.id.button6);


        btn_navigasi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                gmmIntentUri = Uri.parse("google.navigation:q=" + burbacek);


                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);



                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(rumah.this, "Google Maps Belum Terinstal. Install Terlebih dahulu.",
                            Toast.LENGTH_LONG).show();
                }
            }

        });
    }
}
